package com.example.tema1

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.tema1.MyInterface

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Activty2Fragment1.newInstance] factory method to
 * create an instance of this fragment.
 */
class Activty2Fragment1 : Fragment() {

    var myInterface: MyInterface? = null

    companion object {
        fun newInstance() : Fragment {
            return Activty2Fragment1()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when(context) {
            is MyInterface ->
                myInterface= context
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_activty21, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
          val button=view.findViewById<Button>(R.id.button_openFragment2)
        button.setOnClickListener {
            myInterface?.addActivity2Fragment2()
        }
    }
}