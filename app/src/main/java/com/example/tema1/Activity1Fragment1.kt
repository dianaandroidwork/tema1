package com.example.tema1

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.tema1.MyInterface

class Activity1Fragment1 : Fragment() {
    var myInterface: MyInterface? = null

    companion object {
        fun newInstance() : Fragment {
            return Activity1Fragment1()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when(context) {
            is MyInterface ->
                myInterface = context
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_activity11, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val button1=view.findViewById<Button>(R.id.button_fragment1)
        button1.setOnClickListener {
           myInterface?.openActivity2()
        }

        }
    }


