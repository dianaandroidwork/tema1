package com.example.tema1

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button


class Activity2Fragment2 : Fragment() {
    var myInterface: MyInterface? = null

    companion object {
        fun newInstance() : Fragment {
            return Activity2Fragment2()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when(context) {
            is MyInterface ->
                myInterface= context
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_activity22, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val button1=view.findViewById<Button>(R.id.button_openFragment3)
        button1.setOnClickListener {
            myInterface?.replaceWithActivity2Fragment3()
        }
        val button2=view.findViewById<Button>(R.id.button_back)
        button2.setOnClickListener {
            myInterface?.goToActivity2Fragment1()
        }
        val button3=view.findViewById<Button>(R.id.button_Close)
        button3.setOnClickListener {
            myInterface?.close()
        }
    }
}