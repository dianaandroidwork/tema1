package com.example.tema1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.tema1.MyInterface

class MainActivity2 : AppCompatActivity(), MyInterface {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
          addActivity2Fragment1()
    }

    fun addActivity2Fragment1()
    {
        val fragmentManager = supportFragmentManager

        val transaction = fragmentManager.beginTransaction()

        val firstFragment = Activty2Fragment1.newInstance()

        val TAG = Activty2Fragment1::class.java.name

        val addTransaction = transaction.add(
                R.id.frame_layout,
                firstFragment,
                TAG
        )

        addTransaction.addToBackStack(TAG)

        addTransaction.commit()
    }


 override fun addActivity2Fragment2()
 {
     val fragmentManager = supportFragmentManager

     val transaction = fragmentManager.beginTransaction()

     val secondFragment = Activity2Fragment2.newInstance()

     val TAG = Activity2Fragment2::class.java.name

     val replaceTransaction = transaction.add(
             R.id.frame_layout,
             secondFragment,
             TAG
     )

     replaceTransaction.commit()
 }

    override fun replaceWithActivity2Fragment3()
    {
        val fragmentManager=supportFragmentManager
        val transaction=fragmentManager.beginTransaction()
        val tag= Activity2Fragment3::class.java.name
        val replaceTransaction=transaction.replace(
                R.id.frame_layout,
                Activity2Fragment3.newInstance(),
                tag
        )

        replaceTransaction.commit()
    }

    override fun goToActivity2Fragment1()
    {
        val fragmentManager=supportFragmentManager
        fragmentManager.popBackStackImmediate()
    }

  override  fun close()
    {
        this.finish()
    }

    override fun openActivity2() {
        TODO("Not yet implemented")
    }


}