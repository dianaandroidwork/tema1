package com.example.tema1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity(), MyInterface {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }



    override fun openActivity2() {
        val intent: Intent = Intent(this, MainActivity2::class.java)
        startActivity(intent)
         finish()
    }

    override fun addActivity2Fragment2() {
        TODO("Not yet implemented")
    }

    override fun goToActivity2Fragment1() {
        TODO("Not yet implemented")
    }

    override fun replaceWithActivity2Fragment3() {
        TODO("Not yet implemented")
    }

    override fun close() {
        TODO("Not yet implemented")
    }

}